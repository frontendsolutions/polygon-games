/**
 * Ein Basis-Polygon F kann (erst einmal) von einem der beiden Typen sein: gleichseitiges Dreieck oder Quadrat
 *
 * Ich verstehe unter einem Polygon eine Fläche, die durch n-gerichte Kanten begrenzt ist.
 * Diese möchte ich mit Vektoren ausdrücken. Diese können dann sowohl als Länge und Winkel gegenüber der positiven
 * x-Achse aufgefasst werden, als auch als Koordinaten-Paar, d.h. Punkte.
 *
 * Diese Kanten verbinden die n-Eckpunkte des Polygons:  * Ich denke mir immer den untersten linkesten Punkt
 * als Punkt P_1 und nummeriere die anderen gegen den Uhrzeigersinn weiter durch.
 * Die Kante, die die Punkte P1 und P2 verbindet, nenne ich k1.
 *
 *Die Menge aller Punkte aller Polygone zusammen mit allen (diesmal ungerichteten) Kanten kann als Graph aufgefasst
 * werden (jedoch mit der zusätzlichen Eigenschaft, dass die Punkte konkrete Koordinaten für die Darstellung haben)
 *
 * Jede Kante k_index gehört sowohl zu meinem Polygon als auch zu seinem potentiellen Nachbarn, den ich daher
 * abalog dazu N_index nenne und die Betrachtungen rekursiv auf diesen als neues F auffasse.
 *
 * Die Nachbarschaftsrelation zwischen Flächen, kann auch als die graphentheoretische Darstellung des dualen Graphen
 * aufgefast werden. Die neuen Vertices sind dann die Mittelpunkte (Lot, Seitenhalbierende, Winkelhalbierende,
 * Schwerpunkt, ... ?) der Flächen. Und die neuen Kanten stehen mittelsenkrecht auf den alten und verbinden die
 * geeigneten Vertices
 *
 *
 * ====
 * Später kann man mehrere Polygone nach Wahl zu neuen zusammengesetzten Tiles auffassen und alle Betrachtungen
 * darauf geeignet erweitern
 *
 * ===
 *
 * Man kann im View auch darüber nachdenken den Graphen und seinen dualen gleichzeitig darzustellen, geeignet
 * skaliert mit beiden Polygonformen verkleinert oder überlappend ... animiert usw.
 *
 * Hierbei kommt es dann ganz wesentlich darauf an was genau ein neues molekulares Tile darstellt und wie man den
 * virtuellen Mittelpunkt findet um den herum man skaliert. Ein Basis-Polygon ist ein atomares Tile.
 * Frage: Ist ein Sechseck, eine Raute, ein Kite und Cairo-Tile atomar oder molekular? Oder ist das nur
 * kontextabhängig zu entscheiden/zu wählen?
 *
 *
 */
function createBaseTriangle(translate = point(0, 0), startingAngle = 0, length = 1.0) {
    let temp = {
        //name: write the vertex notation down or the schlaefli symbol
        type: 'triangle',
        translate: translate,
        edges: [vector(0.0 + startingAngle, length),
            vector(120.0 + startingAngle, length),
            vector(240.0 + startingAngle, length)
        ],
        points: null,
        neighbours: [null, null, null]
    }
    temp.points = calculatePoints(translate, temp.edges)
    return temp
}

function createBaseSquare(translate = point(0, 0), startingAngle = 0.0, length = 1.0) {
    let temp = {
        //name: write the vertex notation down or the schlaefli symbol
        type: 'square',
        translate: translate,
        edges: [vector(0.0 + startingAngle, length),
            vector(270.0 + startingAngle, length),
            vector(180.0 + startingAngle, length),
            vector(90.0 + startingAngle, length)
        ],
        points: null,
        neighbours: [null, null, null]
    }
    temp.points = calculatePoints(translate, temp.edges)
    return temp
}


//createCairoTile
//createKite

function calculatePoints(translate, edges) {
    let relativeToPoint = translate
    return edges.map((edge) => {
            relativeToPoint = mapVectorToPoint(edge, relativeToPoint)
            return relativeToPoint
        }
    )
}


// function findRightMostPoint(polygon) {
//
// }

// addPolygonToPolygon asNeighbourIfPossible

 