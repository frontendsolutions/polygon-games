function drawTile(tile, parent) {
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    // ToDo hierfuer Sinnvole Werte finden und im Model Vorkonfigurieren iwie
    svg.setAttribute('width', '100px')
    svg.setAttribute('height', '100px')
    svg.setAttribute('viewBox', '-10 -10 20 20')
    svg.innerHTML = '<path d="' + translatePointsToSvgPathD(tile.points) + '" ' 
        + (tile.type === 'triangle' ? ' fill="green" ' : ' fill="blue" ')
        + 'stroke="black" stroke-width="0.3px"></path>'
    
    parent.appendChild(svg)
}

function translatePointsToSvgPathD(points) {
    let retVal = ''
    let bFirst = true
    points.forEach(
        (point) => {
            retVal += `${bFirst ? 'M' : ' L'} ${point.x} ${point.y}`
            bFirst = false
        }
    )
    retVal += ' Z'
    console.log(retVal)
    return retVal
}
