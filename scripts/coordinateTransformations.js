/**
 *
 * 
 */
function toBogenmass(deg) {
    return (2 * Math.PI * deg) / 360
}
function toDegree(rad) {
    (rad * 360) / (2 * Math.PI )
}

function point(x,y) {
    return {x: x, y: y}
}

function vector(phi, length) {
    return {
        phi: normalizeAngle(phi),
        length: length
    }
}
function normalizeAngle(phi) {
    return (1*phi + 360) % 360
}


function mapVectorToPoint(vector, trans = point(0,0)) {
    let bogenmass = toBogenmass(vector.phi)
    let x = Math.sin(bogenmass)
    let y = Math.cos(bogenmass)
    let len = vector.length
    console.log(vector.phi, vector.length,' --->> ',  x, y, ' rad= ', bogenmass)
    return point(len * x + trans.x, len * y + trans.y)
}
function mapPointToVector(inPoint, origin = point(0,0)) {
    let length = distance(inPoint, origin)
    let difference = point((inPoint.x - origin.x) / length, (point.y - origin.y) / length)

    // should be exactly the same angle but because of rounding difference take both and use average of them
    let bogenmassX = Math.asin(difference.x)
    let bogenmassY = Math.acos(difference.y)
    console.log(bogenmassX, bogenmassY)
// ToDo check if it('should is really necessary
    let average = (bogenmassX + bogenmassY) / 2
    let degree = toDegree(average)
    return vector(degree, length)
}

function distance(p1,p2 = point(0,0)) {
    let xSquare = (p1.x - p2.x) ** 2
    let ySquare = (p1.y -p2.y) ** 2
    return Math.sqrt(xSquare + ySquare)
}

function sumPoints(p1,p2) {
    let temp = point(p1.x + p2.x, p1.y + p2.y)
    // console.log("sum of points ", p1, " and ", p2, " is ", temp)
    return temp
}

// function sumPointVector(pt, vec) {
//     return sumPoints(pt, mapVectorToPoint(vec))
// }
 
 